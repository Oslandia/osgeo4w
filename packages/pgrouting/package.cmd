::--------- Package settings --------
:: package name
set P=pgrouting
:: version
set V=2.6.2
:: package version
set B=2

set HERE=%CD%

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1

:: We cheat a bit: we steal the binary from PostGIS bundle ...

set A=postgis-bundle-pg11-2.5.3x64
if not exist %A%.zip (
wget http://download.osgeo.org/postgis/windows/pg11/%A%.zip || goto :error
)
if not exist %A% (
unzip %A%.zip || goto :error
)
c:\cygwin64\bin\tar -C %A% -cjvf %PKG_BIN% ^
 share/extension/pgrouting.control ^
 share/extension/pgrouting--2.0.0--2.6.2.sql ^
 share/extension/pgrouting--2.0.1--2.6.2.sql ^
 share/extension/pgrouting--2.1.0--2.6.2.sql ^
 share/extension/pgrouting--2.2.0--2.6.2.sql ^
 share/extension/pgrouting--2.2.1--2.6.2.sql ^
 share/extension/pgrouting--2.2.2--2.6.2.sql ^
 share/extension/pgrouting--2.2.3--2.6.2.sql ^
 share/extension/pgrouting--2.2.4--2.6.2.sql ^
 share/extension/pgrouting--2.3.0--2.6.2.sql ^
 share/extension/pgrouting--2.3.1--2.6.2.sql ^
 share/extension/pgrouting--2.3.2--2.6.2.sql ^
 share/extension/pgrouting--2.4.0--2.6.2.sql ^
 share/extension/pgrouting--2.4.1--2.6.2.sql ^
 share/extension/pgrouting--2.4.2--2.6.2.sql ^
 share/extension/pgrouting--2.5.0--2.6.2.sql ^
 share/extension/pgrouting--2.5.1--2.6.2.sql ^
 share/extension/pgrouting--2.5.2--2.6.2.sql ^
 share/extension/pgrouting--2.5.3--2.6.2.sql ^
 share/extension/pgrouting--2.5.4--2.6.2.sql ^
 share/extension/pgrouting--2.5.5--2.6.2.sql ^
 share/extension/pgrouting--2.6.0--2.6.2.sql ^
 share/extension/pgrouting--2.6.1--2.6.2.sql ^
 share/extension/pgrouting--2.6.2.sql ^
 lib/libpgrouting-2.6.dll ^
 bin/libstdc++-6.dll ^
 bin/libgcc_s_seh-1.dll || goto :error

::--------- Installation
scp %PKG_BIN% %R% || goto :error
cd %HERE%
scp setup.hint %R% || goto :error

goto :EOF

:error
echo Build failed
exit /b 1
