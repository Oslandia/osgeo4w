::--------- Package settings --------
:: package name
set P=pgtempus
:: version
set V=2.1.0
:: package version
set B=1

set BUILD_DEPS=postgresql boost-vc14 boost-devel-vc14

set CORE_TAG=v3.1.0
set PGTEMPUS_TAG=v%V%

set HERE=%CD%

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1

::--- compile tempus core
if not exist core.tar.bz2 (
wget -O core.tar.bz2 https://gitlab.com/tempus-projects/tempus_core/-/archive/%CORE_TAG%/tempus_core-%CORE_TAG%.tar.bz2 || goto error
)
if not exist tempus_core* (
tar xjvf core.tar.bz2 || goto :error
)
cd tempus_core-* || goto :error
if not exist build mkdir build || goto error
cd build || goto error
cmake -G "NMake Makefiles" ^
  -DCMAKE_BUILD_TYPE=RelWithDebInfo ^
  -DCMAKE_INSTALL_PREFIX=%HERE%\tempus ^
  "-DPOSTGRESQL_PG_CONFIG=C:\osgeo4w64\bin\pg_config.exe" ^
  -DBOOST_LIBRARYDIR=C:\osgeo4w64\lib ^
  -DBOOST_INCLUDEDIR=C:\osgeo4w64\include\boost-1_63 ^
  -DBUILD_DOC=OFF ^
  -DBUILD_TOOLS=OFF ^
  .. || goto error
nmake || goto error
nmake install || goto error
copy /Y lib\tempus.pdb %HERE%\tempus\lib || goto error
cd ..\.. || goto error

::-- copy server include from installed postgresql
if not exist pg_server_include mkdir pg_server_include || goto error
xcopy /S /Y C:\osgeo4w64\include\server\*.* pg_server_include || goto error
cd pg_server_include
patch -N -p0 < ../msvc.patch
cd ..

if not exist pgtempus.tar.bz2 (
wget -O pgtempus.tar.bz2 https://gitlab.com/tempus-projects/tempus_pg/-/archive/%PGTEMPUS_TAG%/tempus_pg-%PGTEMPUS_TAG%.tar.bz2 || goto error
)
if not exist tempus_pg* (
tar xjvf pgtempus.tar.bz2 || goto :error
)
cd tempus_pg-* || goto :error
if not exist build mkdir build || goto error
cd build || goto error

cmake -G "NMake Makefiles" ^
  -DPG_CONFIG=C:\osgeo4w64\bin\pg_config.exe ^
  -DCMAKE_BUILD_TYPE=RelWithDebInfo ^
  -DBoost_INCLUDE_DIR=C:\osgeo4w64\include\boost-1_63 ^
  -DBoost_LINK_DIRECTORY=C:\osgeo4w64\lib ^
  -DTEMPUS_INCLUDE_DIRECTORY=%HERE%\tempus\include ^
  -DPGSQL_INCLUDEDIR_SERVER=%HERE%\pg_server_include ^
  -DPGSQL_LINK_DIRECTORY=C:\osgeo4w64\lib ^
  -DTEMPUS_LINK_DIRECTORY=%HERE%\tempus\lib ^
  .. || goto error
nmake || goto error
if not exist %HERE%\tempus\share\extension (md %HERE%\tempus\share\extension) || goto error
if not exist %HERE%\tempus\bin (md %HERE%\tempus\bin) || goto error
move %HERE%\tempus\lib\tempus.dll %HERE%\tempus\bin || goto error
move %HERE%\tempus\lib\tempus.pdb %HERE%\tempus\bin || goto error
copy /Y pgtempus.dll %HERE%\tempus\lib || goto error
copy /Y pgtempus.pdb %HERE%\tempus\lib || goto error
copy /Y ..\pgext\pgtempus--2.1.sql  %HERE%\tempus\share\extension || goto error
copy /Y ..\pgext\pgtempus.control %HERE%\tempus\share\extension || goto error
cd ..\.. || goto error

cd tempus
tar czvf ../%PKG_BIN% bin/tempus.dll bin/tempus.pdb lib/pgtempus.dll lib/pgtempus.pdb share/extension || goto error
cd ..

::--------- Installation
scp %PKG_BIN% %R% || goto :error
scp setup.hint %R% || goto :error
goto :EOF


:error
echo Build failed
exit /b 1
