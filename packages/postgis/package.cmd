::--------- Package settings --------
:: package name
set P=postgis
:: version
set V=3.0.1
:: package version
set B=5

set HERE=%CD%

set BUILD_DEPS=postgresql geos libxml2 proj

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1

git clone https://gitlab.com/postgis/postgis.git --depth 1 --branch %V% || goto :error

cd %HERE%

::
:: local build of iconv in static
::

git clone https://github.com/winlibs/libiconv.git --depth 1 --branch master || goto :error
cd libiconv\MSVC14
devenv libiconv.sln /Build "Release|x64" /Project libiconv_static

cd %HERE%

:: copy the config file
:: add cmake files

xcopy files\* postgis /SY || goto :error
cd postgis
:: apply C11 patches for MSVC 
for /F %%f in ('dir /b %HERE%\patches') do (
	echo "patch -p0 < %HERE%\patches\%%f"
	patch -p0 < %HERE%\patches\%%f || goto :error
)
mkdir build
cd build

cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo ^
      -DPROJ4_LIBRARY=c:\osgeo4w64\lib\proj.lib ^
      -DPROJ4_INCLUDE_DIR=c:\osgeo4w64\include ^
      -DPOSTGRESQL_LIBRARIES=c:\osgeo4w64\lib\postgres.lib ^
      -DLIBPQ_LIBRARY=c:\osgeo4w64\lib\libpq.lib ^
      -DLIBXML2_LIBRARY=c:\osgeo4w64\lib\libxml2.lib ^
      -DICONV_INCLUDE_DIR=%HERE%\libiconv\source\include ^
      -DICONV_LIBRARY=%HERE%\libiconv\MSVC14\x64\lib\libiconv_a.lib ^
      -DCMAKE_WINDOWS_EXPORT_ALL_SYMBOLS=TRUE ^
	  -DWITH_NLS=BOOL:False ^
      -G "NMake Makefiles" .. || goto :error
	
cmake --build . -- VERBOSE=1 || goto :error

cd %HERE%

:: make sure the install dir is empty before installing

rd /s /q install
mkdir install\lib
mkdir install\share
mkdir install\share\extension
mkdir install\bin

copy postgis\build\postgis\postgis-%V%.dll install\lib || goto :error
copy postgis\build\extensions\postgis\postgis.control install\share\extension || goto :error
copy postgis\build\extensions\postgis\postgis--%V%.sql install\share\extension || goto :error
copy postgis\build\loader\shp2pgsql.exe install\bin || goto :error
copy postgis\build\loader\pgsql2shp.exe install\bin || goto :error

c:\cygwin64\bin\tar.exe -C install -cjvf %PKG_BIN% lib share bin || goto :error

cd %HERE%

::--------- Installation
call %HERE%\..\__inc__\install_archives.bat || goto :error

goto :EOF

:error
echo Build failed
exit /b 1
