::--------- Package settings --------
:: package name
set P=python3-sqlalchemy
:: version
set V=1.3.13
:: package version
set B=1

set HERE=%CD%

set BUILD_DEPS=python3-setuptools

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1
set OSGEO4W_ROOT=c:\osgeo4w64
set PATH=%OSGEO4W_ROOT%\bin;%PATH%

:: python3 package
call %OSGEO4W_ROOT%\bin\py3_env.bat || goto :error

python3 -m pip install sqlalchemy==%V% || goto :error

cd %HERE%

tar -C %OSGEO4W_ROOT% -cvjf %PKG_BIN% apps/Python37/Lib/site-packages/sqlalchemy apps/Python37/Lib/site-packages/SQLAlchemy-%V%-py3.7.egg-info || goto :error

::--------- Installation
scp %PKG_BIN% %R% || goto :error
cd %HERE%
scp setup.hint %R% || goto :error

goto :EOF

:error
echo Build failed
exit /b 1
