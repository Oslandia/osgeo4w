::--------- Package settings --------
:: package name
set P=qgis-albion_data
:: version
set V=2.3.0
:: package version
set B=1
:: plugin name

set HERE=%CD%

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1
set OSGEO4W_ROOT=c:\osgeo4w64
set PATH=%OSGEO4W_ROOT%\bin;%PATH%
set PYTHONPATH=%PYTHONPATH%;%HERE%
set PLUGINDIR=install\share\doc\albion

:: python3 package
call %OSGEO4W_ROOT%\bin\py3_env.bat || goto :error

mkdir -p %PLUGINDIR% || goto :error
wget 'https://gitlab.com/Oslandia/albion_data/-/archive/master/albion_data-master.zip?path=nt' --output-document 'albion_data.zip' || goto :error
unzip -d %PLUGINDIR% albion_data.zip || goto :error
mv %PLUGINDIR%\albion_data-master-nt\nt %PLUGINDIR%\data || goto :error
c:\cygwin64\bin\tar.exe --transform 's,install/,,'  -cvjf %PKG_BIN% install || goto :error

::--------- Installation
scp %PKG_BIN% %R% || goto :error
scp setup.hint %R% || goto :error
goto :EOF

:error
echo Build failed
exit /b 1
