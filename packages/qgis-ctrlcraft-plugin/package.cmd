echo "on entre dans le package" 
::--------- Package settings --------
:: package name
set P=qgis-ctrlcraft-plugin
:: version
set V=2.0.5
:: package version
set B=1

set HERE=%CD%

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1
set OSGEO4W_ROOT=c:\osgeo4w64
set PATH=%OSGEO4W_ROOT%\bin;%PATH%
set PYTHONPATH=%PYTHONPATH%;%HERE%
set PLUGINDIR=%OSGEO4W_ROOT%\apps\qgis-ltr\python\plugins

:: python3 package
call %OSGEO4W_ROOT%\bin\py3_env.bat || goto :error

mkdir apps\qgis-ltr\python\plugins
mkdir etc\postinstall
wget https://gitlab.com/Oslandia/qgis/CtrlCraft/-/archive/v%V%/CtrlCraft-v%V%.zip || goto :error
unzip CtrlCraft-v%V%.zip || goto :error
rmdir /S /Q apps\qgis-ltr\python\plugins\CtrlCraft
move /Y CtrlCraft-v%V% CtrlCraft
move /Y CtrlCraft apps\qgis-ltr\python\plugins\
copy postinstall.bat etc\postinstall\ctrlcraft_postinstall.bat

7z a -ttar %P%.tar "apps/qgis-ltr" "etc/postinstall" || goto :error
7z a -tbzip2 %PKG_BIN% %P%.tar || goto :error

::--------- Installation
scp %PKG_BIN% %R% || goto :error
scp setup.hint %R% || goto :error
goto :EOF


:error
echo Build failed
exit /b 1
