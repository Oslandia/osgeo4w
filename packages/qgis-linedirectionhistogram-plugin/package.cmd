::--------- Package settings --------
:: package name
set P=qgis-linedirectionhistogram-plugin
:: version
set V=3.1.1
:: package version
set B=1

set HERE=%CD%

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1
set OSGEO4W_ROOT=c:\osgeo4w64
set PATH=%OSGEO4W_ROOT%\bin;%PATH%
set PYTHONPATH=%PYTHONPATH%;%HERE%
set PLUGINDIR=install\apps\qgis-ltr\python\plugins

:: python3 package
call %OSGEO4W_ROOT%\bin\py3_env.bat || goto :error

mkdir -p %PLUGINDIR% || goto :error
wget 'http://plugins.qgis.org/plugins/LineDirectionHistogram/version/%V%/download/' --output-document 'LineDirectionHistogram-%V%.zip' || goto :error
unzip -d %PLUGINDIR% LineDirectionHistogram-%V%.zip || goto :error
c:\cygwin64\bin\tar.exe --transform 's,install/,,'  -cvjf %PKG_BIN% install || goto :error

::--------- Installation
scp %PKG_BIN% %R% || goto :error
scp setup.hint %R% || goto :error
goto :EOF

:error
echo Build failed
exit /b 1
