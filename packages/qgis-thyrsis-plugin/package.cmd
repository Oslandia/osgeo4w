::--------- Package settings --------
:: package name
set P=qgis-thyrsis-plugin
:: version
set V=1.1.1
:: package version
set B=11

set HERE=%CD%

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1
set OSGEO4W_ROOT=c:\osgeo4w64
set PATH=%OSGEO4W_ROOT%\bin;%PATH%
set PYTHONPATH=%PYTHONPATH%;%HERE%

:: python3 package
call %OSGEO4W_ROOT%\bin\py3_env.bat || goto :error

:: python -m pip install sip || goto :error
git clone https://gitlab+deploy-token-228659:Bksq1jENwAPDuDt322rd@gitlab.com/Oslandia/QGIS/thyrsis.git || goto :error

set PYTHONPATH=%PYTHONPATH%;%HERE%
mkdir -p install\apps\qgis-ltr\python\plugins\
mkdir install\bin

python -m thyrsis.package -d -t -i install/apps/qgis-ltr/python/plugins/ || goto :error

wget http://hekla.oslandia.net/thyrsis/win32.zip
unzip win32.zip -d install/bin

mkdir install\apps\porousMultiphaseFoam
wget http://hekla.oslandia.net/thyrsis/git_porousMultiphaseFoam.tar.gz
move git_porousMultiphaseFoam.tar.gz install\apps\porousMultiphaseFoam

mkdir install\etc\postinstall
copy postinstall.bat install\etc\postinstall\thyrsis_plugin_postinstall.bat
:: echo --transform 's,install/,apps/qgis-ltr/python/plugins/,' -cvjf %PKG_BIN% install
c:\cygwin64\bin\tar.exe --transform 's,install/,,' -cvjf %PKG_BIN% install || goto :error


::--------- Installation
scp %PKG_BIN% %R% || goto :error
scp setup.hint %R% || goto :error
goto :EOF


:error
echo Build failed
exit /b 1
